<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'swiper_description' => 'Ce plugin repose sur le plugins Swiper by idangerous porté pour SPIP. Il offre un objet éditorial plein écran ainsi que son modèle à insérer dans vos textes.',
	'swiper_nom' => 'Swiper',
	'swiper_slogan' => 'Un slider à la fois objet éditorial et modèle. Avec gestion des touchdevices',
);
