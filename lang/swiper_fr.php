<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_swiper' => 'Ajouter ce swiper',
	//

	// C
	'champ_titre_label' => 'Titre',
	'cfg_titre_parametrages' => 'Options',
	'confirmer_supprimer_swiper' => 'Confirmez-vous la suppression de ce swiper ?',
	'couleur' => 'Couleur',
	'couleur_bleu' => 'Bleu',
	'couleur_blanc' => 'Blanc',




	// I
	'icone_creer_swiper' => 'Créer un swiper',
	'icone_modifier_swiper' => 'Modifier ce swiper',
	'info_1_swiper' => 'Un swiper',
	'info_aucun_swiper' => 'Aucun swiper',
	'info_nb_swipers' => '@nb@ swiper',
	'info_swipers_auteur' => 'Les swiper de cet auteur',

	// N
	'navigation' => 'Navigation (Flèches)',
	'nom_site'=>'Nom du lien',

	// P
	'pagination' => 'Pagination (Points)',

	// R
	'retirer_lien_swiper' => 'Retirer ce swiper',
	'retirer_tous_liens_swipers' => 'Retirer tous les swiper',

	// S
	'scrollbar' => 'Scrollbar (Barre de défilement)',
	'supprimer_swiper' => 'Supprimer ce swiper',
	'swiper_options' => 'Options manuelles',

	// T
	'texte_ajouter_swiper' => 'Ajouter un swiper',
	'texte_changer_statut_swiper' => 'Ce swiper est :',
	'texte_creer_associer_swiper' => 'Créer et associer un swiper',
	'texte_definir_comme_traduction_swiper' => 'Ce swiper est une traduction du swiper numéro :',
	'titre_langue_swiper' => 'Langue de ce swiper',
	'titre_logo_swiper' => 'Logo de ce swiper',
	'titre_objets_lies_swiper' => 'Liés à ce swiper',
	'titre_page_configurer_swiper' => 'Configuration de vos swipers',
	'titre_swiper' => 'Swiper',
	'titre_swipers' => 'Swiper',
	'titre_swipers_rubrique' => 'Swiper de la rubrique',

	//
	'url_site'=>'Lien',
);
