<?php
/**
 * Utilisations de pipelines par Swiper
 *
 * @plugin     Swiper
 * @copyright  2017
 * @author     Charles Stephan
 * @licence    GNU/GPL
 * @package    SPIP\Swiper\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function swiper_configurateur() {

	$c = lire_config('swiper');
	$flux = '<script type="text/javascript">';
	$flux .= '	const swiper_options = '.$c["swiper_options"];
	$flux .= '	, spipSwipers = {}';
	$flux .= '	, swipersInstantiation = function() { ';
	$flux .= '    var target = swiper_options.containerModifierClass || ".swiper-container";';
	$flux .= '		target = $(target);';
	$flux .= '		if (target.length) target.each(function(i,el) { spipSwipers[i] = new Swiper(el, swiper_options); })';
	$flux .= '	};';
	$flux .= '$(document).ready(swipersInstantiation);';
	$flux .= 'onAjaxLoad(swipersInstantiation)';
	$flux .= '</script>';

	return $flux;
}

function swiper_insert_head($flux) {

	$lib 	= find_in_path('lib/swiper/swiper-bundle.min.js');
	$flux .='<script src="'.$lib.'"	type="text/javascript"></script>';
	$flux .= swiper_configurateur();

	return $flux;
}

function swiper_insert_head_css($flux) {

	$css = find_in_path('lib/swiper/swiper-bundle.min.css');
	$flux .='<link rel="stylesheet" type="text/css" href="'.$css.'">';

	$swiper_spip_css = find_in_path('swiper_spip.css');
	$flux .='<link rel="stylesheet" type="text/css" href="'.$swiper_spip_css.'">';

	return $flux;
}

function swiper_header_prive($flux) {
	$flux .= swiper_insert_head_css('');
	$flux .= swiper_insert_head('');
	return $flux;
}
